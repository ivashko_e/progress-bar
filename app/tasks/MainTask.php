<?php

class MainTask extends \Phalcon\Cli\Task
{
    public function mainAction()
    {
        // Cache data for 2 days
        $frontCache = new \Phalcon\Cache\Frontend\Data(array(
            "lifetime" => 172800,
        ));

        //Create the Cache setting redis connection options
        $cache = new Phalcon\Cache\Backend\Redis($frontCache, array(
            'host' => '127.0.0.1',
            'port' => 6379,
        ));

        for ($i = 1; $i <= 100; $i++) {

            sleep(rand(1, 5));

            //Cache arbitrary data
            $cache->save('data', $i);

            echo $cache->get('data'), PHP_EOL;
        }
    }
}
