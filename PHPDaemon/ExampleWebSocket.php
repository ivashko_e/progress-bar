<?php
namespace PHPDaemon\Applications;

use PHPDaemon\HTTPRequest\Generic;
use PHPDaemon\Core\Daemon;
use PHPDaemon\Core\Debug;

/**
 * @package    Examples
 * @subpackage WebSocket
 *
 * @author     Vasily Zorin <maintainer@daemon.io>
 */
class ExampleWebSocket extends \PHPDaemon\Core\AppInstance
{
    public $sessions = [];

    public function onReady()
    {
        $appInstance = $this;

        $this->timerTask($appInstance);

        \PHPDaemon\Servers\WebSocket\Pool::getInstance()->addRoute('websocket', function ($client) use ($appInstance) {

            $session = new ExampleWebSocketRoute($client, $appInstance); // Создаем сессию
            $session->id = uniqid(); // Назначаем ей уникальный ID
            $this->sessions[$session->id] = $session; //Сохраняем в массив
            return $session;
        });
    }

    function timerTask($appInstance)
    {
        $frontCache = new \Phalcon\Cache\Frontend\Data([
            "lifetime" => 172800,
        ]);

        //Create the Cache setting redis connection options
        $cache = new \Phalcon\Cache\Backend\Redis($frontCache, [
            'host' => '127.0.0.1',
            'port' => 6379,
        ]);

        $data = $cache->get('data');

        foreach ($this->sessions as $id => $session) {

            $session->client->sendFrame($data, 'STRING');
        }

        // Перезапускаем наш метод спустя 1 секунду
        \PHPDaemon\Core\Timer::add(function ($event) use ($appInstance) {
            $this->timerTask($appInstance);
            $event->finish();
        }, 1e6); // Время задается в микросекундах
    }
}

class ExampleWebSocketRoute extends \PHPDaemon\WebSocket\Route
{
    public $client;
    public $appInstance;
    public $idq;

    public function __construct($client, $appInstance)
    {
        $this->client = $client;
        $this->appInstance = $appInstance;
    }

    /**
     * Called when new frame received.
     * @param string  Frame's contents.
     * @param integer Frame's type.
     * @return void
     */
    public function onFrame($data, $type)
    {
        if ($data === 'ping') {
            $this->client->sendFrame('pong', 'STRING');
        }
    }

    /**
     * Uncaught exception handler
     * @param $e
     * @return boolean|null Handled?
     */
    public function handleException($e)
    {
        $this->client->sendFrame('exception ...');
    }
}

