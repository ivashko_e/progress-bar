$(function () {

    var Socket = {
        ws: null,

        init: function () {

            ws = new WebSocket('ws://' + document.location.host + ':8047/websocket');

            ws.onopen = function () {

                console.log('Socket opened');
            };

            ws.onclose = function () {

                console.log('Socket close');
            };

            ws.onmessage = function (e) {

                console.log(e.data);

                if (e.data > 0) {

                    $('div.progress-bar')
                        .css('width', e.data + '%')
                        .attr('aria-valuenow', e.data)
                        .text(e.data + '% Complete (success)');
                }
            };

            this.ws = ws;
        }
    };

    Socket.init();
    var socket = Socket.ws;

    setInterval(function() {

        socket.send('ping');
    },10000);

});